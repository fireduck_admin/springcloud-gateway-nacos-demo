package com.maggie.demo.dick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class DickApplication {

    public static void main(String[] args) {
        SpringApplication.run(DickApplication.class, args);
    }

}
