package com.maggie.demo.dick.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping("/abc")
    public String testTick() {
        return "abc";
    }
}
