package com.maggie.demo.tick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class TickApplication {

    public static void main(String[] args) {
        SpringApplication.run(TickApplication.class, args);
    }

}
